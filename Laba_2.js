function readAPI()
{	
	var url = 'https://archive.org/advancedsearch.php?';
	
	// Тело запроса для archive.org
	var request_data = {
		// Ключевые слова
		'q' : 'SaltStack+AWS+Cloud+Docker+Python',
		// Фильтры
		'fl[]' : 'date,creator,title,description',
		// Способ вывода
		'output' : 'json',
	};
	
	// Формируем ссылку запроса
	var request_url = url;
	Object.keys(request_data).forEach(function (key) {
		request_url = request_url + key + '=' + request_data[key] + '&';
	});
	//request_url = request_url.substr(0, request_url.length - 1);
	
	// Отправляем GET запрос на сервер
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open( "GET", request_url, false );
	xmlHttp.send( null );
	var urlcontent = xmlHttp.responseText;
	
	// Парсим полученный JSON
	var response = JSON.parse(urlcontent);
	
	console.log(JSON.stringify(response, null, 4));
	
	// Чистим таблицу
	var table = document.getElementById("table");
	while (table.rows.length > 1) {
		table.deleteRow(1);
	}
	
	// Заполняем таблицу
	for (var element of response['response']['docs']) {
		row = table.insertRow();
		var cell1 = row.insertCell();
		cell1.innerHTML = element["creator"];
		var cell2 = row.insertCell();
		cell2.innerHTML = element["date"];
		var cell3 = row.insertCell();
		cell3.innerHTML = element["title"];
		var cell4 = row.insertCell();
		cell4.innerHTML = element["description"];
	}
}



















